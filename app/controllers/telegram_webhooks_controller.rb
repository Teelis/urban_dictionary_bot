class TelegramWebhooksController < Telegram::Bot::UpdatesController
  URBAN_DICTIONARY_URL = 'http://api.urbandictionary.com/v0/define'.freeze
  URBAN_TODAY_WORD_URL = 'http://urban-word-of-the-day.herokuapp.com/today'.freeze

  def start!(*)
    respond_with :message, text: t('.hi') + t('.help.commands')
  end

  def help!(*)
    respond_with :message, text: t('.commands')
  end

  def definition!(term)
    response = RestClient.get(
      URBAN_DICTIONARY_URL,
      accept: :json,
      params: {term: term}
    )

    word_definition = JSON.parse(response.body)["list"][0]

    message = [
      "<a href=\"#{word_definition['permalink']}\">#{word_definition['word']}</a>",
      "\n\n",
      word_definition['definition'].delete('[]'),
      "\n\n",
      "<i>#{word_definition['example'].delete('[]')}</i>"
    ].join

    respond_with :message, text: message, parse_mode: "HTML"
  end

  def word_of_the_day!
    response = RestClient.get URBAN_TODAY_WORD_URL, accept: :json

    word_definition = JSON.parse(response.body)

    message = [
      "<b>#{word_definition['word']}</b>",
      "\n\n",
      word_definition['meaning'].delete("\n"),
      "\n\n",
      "<i>#{word_definition['example'].delete("\n")}</i>"
    ].join

    respond_with :message, text: message, parse_mode: "HTML"
  end

  def message(message)
    msg = message['text'].mb_chars.downcase.to_s.split(' ')
    command = msg[0]
    case command
    when '!urban_today'
      word_of_the_day!
    when '!urban'
      term = msg[1..-1].join(' ')
      definition!(term)
    end
  end
end
